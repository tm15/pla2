package ga.manuelgarciacr.tripmemories.ui.main;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ga.manuelgarciacr.tripmemories.model.Trip;
import ga.manuelgarciacr.tripmemories.model.TripRepository;

public class MainViewModel extends AndroidViewModel {
    private LiveData<List<Trip>> mAllTrips;

    public MainViewModel(@NonNull Application application) {
        super(application);
        TripRepository tripRepository = new TripRepository(application);
        mAllTrips = tripRepository.getAllTrips();
    }

    LiveData<List<Trip>> getAllTrips() {
        return mAllTrips;
    }
}
