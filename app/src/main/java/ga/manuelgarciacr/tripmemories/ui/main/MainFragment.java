package ga.manuelgarciacr.tripmemories.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.R;
import ga.manuelgarciacr.tripmemories.model.Trip;

public class MainFragment extends Fragment {
    private List<Trip> mAllTrips = new ArrayList<>();
    private View mView;
    private Callbacks mCallbacks = null;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        //noinspection ConstantConditions
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");
        Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(false);

        return (mView = inflater.inflate(R.layout.main_fragment, container, false));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
         RecyclerView recyclerView = mView.findViewById(R.id.recyclerview);
        final TripListAdapter adapter = new TripListAdapter(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        MainViewModel mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mViewModel.getAllTrips().observe(this, new Observer<List<Trip>>() {
            @Override
            public void onChanged(@Nullable final List<Trip> trips) {
                adapter.setTrips(trips);
            }
        });
        adapter.setTrips(mAllTrips);
        FloatingActionButton fab = mView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallbacks.onFabClick();
            }
        });
    }

    public interface Callbacks {
        void onTripSelected(UUID trip);
        void onFabClick();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public class TripListAdapter extends RecyclerView.Adapter<TripListAdapter.TripViewHolder>{
        private final LayoutInflater mInflater;
        private List<Trip> mAllTrips;

        class TripViewHolder extends RecyclerView.ViewHolder{
            private final TextView tripName;

            TripViewHolder(@NonNull View itemView) {
                super(itemView);
                tripName = itemView.findViewById(R.id.textView);
            }
        }

        TripListAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        void setTrips(List<Trip> trips) {
            mAllTrips = trips;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public TripListAdapter.TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
            itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v){
                    String txt  = (String) ((TextView)v.findViewById(R.id.textView)).getText();
                    Snackbar.make(v, txt, Snackbar.LENGTH_LONG)
                            .show();
                }
            });
            return new TripListAdapter.TripViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull TripListAdapter.TripViewHolder holder, int position) {
            final Trip current = mAllTrips.get(position);
            holder.tripName.setText(current.getName());
            holder.tripName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallbacks.onTripSelected(current.getUUID());
                }
            });
        }

        @Override
        public int getItemCount() {
            if (mAllTrips != null)
                return mAllTrips.size();
            else return 0;
        }
    }
}
