package ga.manuelgarciacr.tripmemories.ui.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import java.util.Objects;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.R;
import ga.manuelgarciacr.tripmemories.model.Trip;

public class DetailFragment extends Fragment {
    private static final String ARG_TRIP_ID = "trip_id";
    private View mView;
    private UUID mUuid;
    private Trip mTrip = null;

    public static DetailFragment newInstance(UUID tripId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRIP_ID, tripId);
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        mUuid = (UUID) getArguments().getSerializable(ARG_TRIP_ID);
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setSubtitle(mUuid.toString());
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        //Toast.makeText(getActivity(),"Aquest és el id: " + mUuid, Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return (mView = inflater.inflate(R.layout.detail_fragment, container, false));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DetailViewModel mViewModel = ViewModelProviders.of(this).get(DetailViewModel.class);
        mViewModel.tripLiveData.observe(getViewLifecycleOwner(), trip -> {
            mTrip = trip;
            ((TextView)mView.findViewById(R.id.txvTripCountry)).setText(mTrip.getCountry());
            ((EditText)mView.findViewById(R.id.edtTripName)).setText(mTrip.getName());
        });
        mViewModel.loadUUID(mUuid);
    }

}
