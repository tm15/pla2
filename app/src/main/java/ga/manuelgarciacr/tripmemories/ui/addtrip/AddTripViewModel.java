package ga.manuelgarciacr.tripmemories.ui.addtrip;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import org.jetbrains.annotations.NotNull;

import ga.manuelgarciacr.tripmemories.model.TripRepository;

public class AddTripViewModel extends AndroidViewModel {
    private TripRepository tripRepository;

    public AddTripViewModel(@NonNull Application application) {
        super(application);
        tripRepository = new TripRepository(application);
    }

    void insert(@NotNull String name, @NotNull String country){
        tripRepository.insert(name, country);
    }
}
