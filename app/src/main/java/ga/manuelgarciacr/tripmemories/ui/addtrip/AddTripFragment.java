package ga.manuelgarciacr.tripmemories.ui.addtrip;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import ga.manuelgarciacr.tripmemories.R;

public class AddTripFragment extends Fragment {

    private AddTripViewModel mViewModel;
    private View mView;

    public static AddTripFragment newInstance() {
        return new AddTripFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return (mView = inflater.inflate(R.layout.add_trip_fragment, container, false));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AddTripViewModel.class);
        final EditText mCountry = mView.findViewById(R.id.edtAddTripCountry),
                mName = mView.findViewById(R.id.edtAddTripName);

        final Button button = mView.findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Boolean countryIsEmpty = TextUtils.isEmpty(mCountry.getText());
                Boolean nameIsEmpty = TextUtils.isEmpty(mName.getText());
                if (countryIsEmpty || nameIsEmpty) {
                    Snackbar.make(view, R.string.empty_not_saved, Snackbar.LENGTH_LONG)
                            .show();
                } else {
                    mViewModel.insert(mName.getText().toString(), mCountry.getText().toString());
                    Objects.requireNonNull(getFragmentManager()).popBackStackImmediate();
                }
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

}
