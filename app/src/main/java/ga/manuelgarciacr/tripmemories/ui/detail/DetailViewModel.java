package ga.manuelgarciacr.tripmemories.ui.detail;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.UUID;

import ga.manuelgarciacr.tripmemories.model.Trip;
import ga.manuelgarciacr.tripmemories.model.TripRepository;

public class DetailViewModel extends AndroidViewModel {
    private TripRepository tripRepository;
    private MutableLiveData<UUID> tripIdLiveData = new MutableLiveData<>();
    LiveData<Trip> tripLiveData =
            Transformations.switchMap(tripIdLiveData, mUUID ->
                    tripRepository.getTrip(mUUID));

    public DetailViewModel(@NonNull Application application) {
        super(application);
        tripRepository = new TripRepository(application);
    }

    void loadUUID(UUID mTripId) {
        tripIdLiveData.setValue(mTripId);
    }

}
