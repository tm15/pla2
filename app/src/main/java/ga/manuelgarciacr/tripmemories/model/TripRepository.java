package ga.manuelgarciacr.tripmemories.model;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

public class TripRepository {
    private final TripDao mTripDao;
    private final LiveData<List<Trip>> mAllTrips;

    public TripRepository(Application application) {
        TripDatabase db = TripDatabase.getDatabase(application);
        mTripDao = db.tripDao();
        mAllTrips = mTripDao.getTrips();
    }

    public LiveData<List<Trip>>  getAllTrips() {
        return mAllTrips;
    }

    public LiveData<Trip>  getTrip(UUID uuid) {
        return mTripDao.getTrip(uuid);
    }

    private static class InsertAsyncTask extends AsyncTask<Trip, Void, Void> {

        private TripDao asyncTaskDao;

        InsertAsyncTask(TripDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Trip... params) {
            asyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void insert(@NotNull String name, @NotNull String country){
        InsertAsyncTask task = new InsertAsyncTask(mTripDao);
        task.execute(new Trip(name, country));
    }
}
