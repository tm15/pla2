package ga.manuelgarciacr.tripmemories.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;
import java.util.UUID;

@Dao
public interface TripDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Trip trip);

    @Query("DELETE FROM table_trips")
    void deleteAll();

    @Query("SELECT * FROM table_trips")
    LiveData<List<Trip>> getTrips();

    @Query("SELECT * FROM table_trips WHERE id = :id")
    LiveData<Trip> getTrip(UUID id);
}
