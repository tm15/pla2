package ga.manuelgarciacr.tripmemories.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.UUID;

@Entity(tableName = "table_trips")
public class Trip {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private UUID UUID;
    @NonNull
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "country")
    private String country;

    Trip(@org.jetbrains.annotations.NotNull String name, String country) {
        UUID = java.util.UUID.randomUUID();
        this.name = name;
        this.country = country;
    }

    @NonNull
    public UUID getUUID() {
        return UUID;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    void setUUID(@NonNull java.util.UUID UUID) {
        this.UUID = UUID;
    }
}
