package ga.manuelgarciacr.tripmemories;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.ui.addtrip.AddTripFragment;
import ga.manuelgarciacr.tripmemories.ui.detail.DetailFragment;
import ga.manuelgarciacr.tripmemories.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity implements MainFragment.Callbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow();
        }
    }

    @Override
    public void onTripSelected(UUID trip) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, DetailFragment.newInstance(trip))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onFabClick() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, AddTripFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
